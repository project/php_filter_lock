Description:
---------------------------
PHP Filter Lock is a security module for Drupal. It disables the ability to edit text fields using the PHP input format until that text format is changed to another format.

In other words, it blocks users from creating new PHP content in text fields but still permits existing PHP content to load from the database.



Notes:
---------------------------
Note that the PHP input filter must be named "PHP code" in your database or this module will not work. A small patch is included that extends the core filter module to allow for specific input filter detection.



Authors:
---------------------------
PHP Filter Lock is supported and maintained by the Larks at Exaltation of Larks.

Sebastian Chedal
http://drupal.org/user/731966

Christefano
http://drupal.org/user/104